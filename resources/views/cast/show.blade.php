@extends('master')

@section('judul')
Halaman Detail Cast {{$cast->nama}}
@endsection

@section('content')
    <h4>Nama : {{$cast->nama}}</h4>
    <h4>Umur : {{$cast->umur}}</h4>
    <p>{{$cast->bio}}</p>
@endsection